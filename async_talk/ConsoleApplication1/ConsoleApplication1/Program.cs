﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("starting on thread: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine("enter something");


            ThreadPool.QueueUserWorkItem(a => DoIOWork());

            var ch = string.Empty;
            while (!String.Equals(ch, "q"))
            {
                ch = Console.ReadLine();
                Console.WriteLine("you entered:" + ch);
            }

            Console.WriteLine("ending on thread: " + Thread.CurrentThread.ManagedThreadId);
            Console.ReadLine();
        }

        private static void DoIOWork()
        {
            var webclient = new WebClient();
            string content = webclient.DownloadString("http://localhost:21111/api/values");
        }

        private static void temp()
        {
            ThreadPool.QueueUserWorkItem(_ => Utils.DoSlowStuff(5));

            var ch = string.Empty;
            while (!String.Equals(ch, "q"))
            {
                ch = Console.ReadLine();
                Console.WriteLine("you entered:" + ch);
            }
        }
    }
}

//https://bitbucket.org/xenry/miscpublic