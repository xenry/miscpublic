﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    internal class Utils
    {
        private static Random rnd = new Random();

        public static int DoSlowStuff(int multiplier)
        {
            Console.WriteLine("doing slow stuff on thread: " + Thread.CurrentThread.ManagedThreadId);

            int primesCnt = 0;
            for (int i = 0; i < multiplier; i++)
            {
                int number = rnd.Next((int)Math.Floor(Int32.MaxValue / 2d), Int32.MaxValue);

                if (IsPrime(number)) primesCnt++;
            }

            Console.WriteLine("finished doing slow stuff on thread: " + Thread.CurrentThread.ManagedThreadId);

            return primesCnt;
        }

        public static bool IsPrime(int number)
        {
            bool isPrime = true;
            for (int i = 2; i <= number; ++i)
            {
                if (number % i == 0)
                {
                    isPrime = false;
                }
            }

            return isPrime;
        }
    }
}
